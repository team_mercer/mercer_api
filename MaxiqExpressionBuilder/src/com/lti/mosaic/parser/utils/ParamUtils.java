package com.lti.mosaic.parser.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ParamUtils {
	
	private static final Logger logger = LoggerFactory.getLogger(ParamUtils.class);

	private ParamUtils() {
	}
	
	public static String getString(Object... values) {
		if (values == null)
			return "";

		StringBuilder rec = new StringBuilder("");
		try {
			for (Object value : values) {
				if (value != null)
					rec.append(value.toString()).append(" ");
			}	
		} catch (Exception e) {
			logger.error("{}",e.getMessage());
		}
		
		return rec.toString();
	}

}
