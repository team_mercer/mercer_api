package com.lti.mosaic.parser.exception;


/**
 * Licensed Information -- need to work on this 
 */

/**
 * Expception - In case of invalid param 
 * <p/>
 * 
 * @author Piyush
 * @since 1.0
 * @version 1.0
 */


public class InvalidParamException extends ExpressionEvaluatorException {
	
	private static final long serialVersionUID = 1L;

	public InvalidParamException(String message, Throwable throwable) {
		super(message, throwable);
	}

	public InvalidParamException(String message) {
		super(message);
	}
}
