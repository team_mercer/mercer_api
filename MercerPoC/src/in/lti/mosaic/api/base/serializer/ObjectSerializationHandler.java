package in.lti.mosaic.api.base.serializer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;


/**
 * Serialize and deserialize the object into JSON string
 *
 * @author shiva
 *
 */
public class ObjectSerializationHandler {
  /**
   * This will create the json string for the input object
   *
   * @param o
   * @return
   */


	private ObjectSerializationHandler() {
		throw new IllegalStateException("ObjectSerializationHandler class");
	}

	private static final Logger logger = LoggerFactory.getLogger(ObjectSerializationHandler.class);

  public static String toString(Object o) {
    ObjectMapper mapper = new ObjectMapper();

    // mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS,

    String value=writeValueAsString(o, mapper);
    return value;
  }

  /**
   * This method is used to store date format correctly not in long format
   * 
   * @param o
   * @return
   */
  public static String toStringForRunQuery(Object o) {
    ObjectMapper mapper = new ObjectMapper();

    mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));

    // mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS,

    String value=writeValueAsString(o, mapper);
    return value;

  }

private static String writeValueAsString(Object o, ObjectMapper mapper) {
	try {
      String writeValueAsString = mapper.writeValueAsString(o);

      return writeValueAsString;
    } catch (JsonGenerationException e) {
    	e.printStackTrace();
    } catch (JsonMappingException e) {
    	e.printStackTrace();
    } catch (IOException e) {
    	e.printStackTrace();
    }
	return "";
}

  /**
   * This will create the object back from the json string.
   *
   */

  public static Object toObject(String str, Class<?> className) {
    ObjectMapper mapper = new ObjectMapper();
    mapper.configure(Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER, true);

    mapper.configure(Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);

    mapper.configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);

    mapper.configure(Feature.ALLOW_SINGLE_QUOTES, true);

    mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

    mapper.setSerializationInclusion(Include.NON_NULL);

    try {
    	return mapper.readValue(str, className);
    } catch (Exception e) {
      str = "\"" + str + "\"";
      try {

    	  return mapper.readValue(str, className);
      } catch (Exception e1) {
    	  logger.info(e1.getMessage());
      }
    }
    return null;
  }

  public static <T> List<T> toObjectList(String str, Class className)
      throws IOException {
    List<T> list = null;
    ObjectMapper mapper = new ObjectMapper();
    TypeFactory factory = TypeFactory.defaultInstance();

    list = mapper.readValue(str, factory.constructCollectionType(ArrayList.class, className));
    return list;
  }

  public static Map convertJsonToMap(String json) throws IOException {
    ObjectMapper mapper = new ObjectMapper();

    mapper.configure(JsonParser.Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER, true);

    mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);

    mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);

    mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);

    mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

    mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

    return mapper.readValue(json, new TypeReference<HashMap>() {});

  }

  public static Map convertLoggerStringToMap(String loggerStringJson) {
    Map<String, String> loggerStingToJson =
        (Map<String, String>) ObjectSerializationHandler.toObject(loggerStringJson, Map.class);

    return loggerStingToJson;
  }

  public static String convertLoggerMapToString(Map<String, String> loggerJson) {
    String loggerJsonToString = ObjectSerializationHandler.toString(loggerJson);

    return loggerJsonToString;
  }
}
